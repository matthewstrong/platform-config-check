import pytest_check as check
import requests
import hashlib
import os
import logging
from pprint import pformat

def get_cf_signed_url(base_url, credentials, filename, filesize, direction):
    url = "https://{0}/service/file-transfer-service/filetransfer/url".format(base_url)
    body = {
        "direction": direction, "filename": filename, "filesize": filesize
    }
    logging.info("Request Body: %s", pformat(body))
    r = requests.post(url, json=body, auth=credentials)
    assert 200 == r.status_code

    payload = r.json()
    logging.info("Response Body: %s", pformat(payload))
    return payload['url']

def get_multipart_signed_url(base_url, credentials, filename, nparts):
    url = "https://{0}/service//file-transfer-service/filetransfer/generatemultipartuploadurls?fileName={1}&partCount={2}".format(base_url, filename, nparts)
    r = requests.post(url, auth=credentials)
    assert 200 == r.status_code
    payload = r.json()
    return payload

def test_cloudfront_upload(config, credentials):
    file_to_upload = 'data/TestUpload20Mb.bin'
    file_size = os.stat(file_to_upload)
    url = get_cf_signed_url(config['url'], credentials, "TestUpload20Mb.bin", 20971520, "upload")
    with open(file_to_upload, 'rb') as f:
        r = requests.put(url, data=f)
    assert 200 == r.status_code

def test_cloudfront_download(config, credentials):
    url = get_cf_signed_url(config['url'], credentials, "TestDownload20Mb.bin", 1000, "download")
    r = requests.get(url)
    assert 200 == r.status_code

    expected_hash = hashlib.md5(open('data/TestDownload20Mb.bin', 'rb').read()).hexdigest()
    actual_hash = hashlib.md5(r.content).hexdigest()
    check.equal(expected_hash, actual_hash)
    logging.info("File hash: %s", actual_hash)

def test_multipart_upload(config, credentials):
    resp = get_multipart_signed_url(config['url'], credentials, "TestUpload20Mb.bin", 3);
    logging.info("Response Data: %s", pformat(resp))

    check.is_in('UploadID', resp)
    check.is_in('Complete', resp)
    check.is_in('Abort', resp)
    check.is_in('Part 1', resp)
    check.is_in('Part 2', resp)
    check.is_in('Part 3', resp)
    check.is_in('s3-accelerate.amazonaws.com', resp['Complete'])