# platform-config-check
Unit test suite for verifying configuration of an SAG Cumulocity tenant.  

Validates the following:
- Proper applications / versions are installed
- Tenant option and values are correct
- CA Certificates for device mutual auth are installed

![This is an image](example_run.png)

# Usage Instructions
## Setup python environment
1. Setup virtualenv and install required packages
   
       python3 -m venv testenv
       source testenv/bin/activiate
       pip3 install -r requirements.txt

## Configure Credentials  
1. Copy secrets file template
   
        cp config/secrets-template.json config/secrets.json
2. Add username / password into file
3. DO NOT commit this file to git
    
## Upload test data to S3
1. Upload data/TestDownload20Mb.bin to S3 downloads bucket

## Running Tests
1. Activate virtualenv
   
        source testeenv/bin/activate
2. Execute test suite with desired config file
   
        pytest --config config/uroph-sandbox.json -v