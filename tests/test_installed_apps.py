import pyjson5
import pytest
import pytest_check as check
import requests
import logging
from common import copy_left_recursive, check_subset_of
from pprint import pformat

@pytest.fixture
def installed_applications(config, credentials):
    url = "https://{base_url}/application/applicationsByOwner/{tenant_id}?pageSize=1000".format(base_url=config['url'], tenant_id=config['tenant_id'])
    r = requests.get(url, auth=credentials)
    assert 200 == r.status_code
    payload = r.json()
    check.is_in('applications', payload)
    applications = payload['applications']
    return applications

def test_app_installed(config, installed_applications, app_name):
    properties_expected = config['applications'][app_name]
    matching = list(filter(lambda a: a["name"] == properties_expected["name"], installed_applications))
    logging.info("Application Details: %s", pformat(matching[0]))

    check.equal(len(matching), 1)
    check_subset_of(properties_expected, matching[0])

    filtered = copy_left_recursive(properties_expected, matching[0])
    check.equal(properties_expected, filtered)

@pytest.mark.production
def test_no_extra_apps(config, installed_applications):
    expected_app_list = config['applications']
    invalid_apps = list()
    for a in installed_applications:
        for b in expected_app_list.values():
            c = copy_left_recursive(b, a)
            if c == b: break
        else:
            invalid_apps.append(a["name"])

    check.equal(list(), invalid_apps)
