import pyjson5
import pytest
import pytest_check as check

def check_subset_of(left, right, path=""):
    for k in left:
        p1 = "{} -> '{}'".format(path, k)
        check.is_in(k, right, "Missing key {}".format(p1))
        if k in right:
            if type(left[k]) is dict:
                check_subset_of(left[k], right[k], p1)
            else:
                check.equal(left[k], right[k], "Value does not match: {}".format(p1))

def copy_left_recursive(left, right):
    copy = dict()
    for k in left:
        if k in right:
            if type(left[k]) is dict:
                copy[k] = copy_left_recursive(left[k], right[k])
            else:
                copy[k] = right[k]
    return copy