import pyjson5
import pytest
import pytest_check as check
import requests
from common import copy_left_recursive, check_subset_of
import logging
from pprint import pformat

def retrieve_trusted_certificate_list(base_url, credentials, tenant_id):
    url = 'https://{base_url}/tenant/tenants/{tenant_id}/trusted-certificates?pageSize=1000'.format(base_url=base_url, tenant_id=tenant_id)
    r = requests.get(url, auth=credentials)
    assert 200 == r.status_code
    payload = r.json()
    check.is_in('certificates', payload)
    certs = payload['certificates']
    return certs

def find_matching_cert(base_url, tenant_id, credentials, certificate_properties):
    cert_list = retrieve_trusted_certificate_list(base_url, credentials, tenant_id)
    logging.info("Trusted Certificates: %s", pformat(cert_list))

    matching = list()
    for cert in cert_list:
        if(certificate_properties.items() <= cert.items()):
            matching.append(cert)
    return matching

def test_trusted_certs(config, credentials, trusted_cert):
    certificates = config['trusted-certificates']
    matching = find_matching_cert(config['url'], config['tenant_id'], credentials, certificates[trusted_cert])
    check.equal(len(matching), 1)

@pytest.mark.production
def test_no_extra_certs(config, credentials):
    expected_cert_list = config['trusted-certificates']
    actual_cert_list = retrieve_trusted_certificate_list(config['url'], credentials, config['tenant_id'])
    invalid_certs = list()
    for a in actual_cert_list:
        for b in expected_cert_list.values():
            c = copy_left_recursive(b, a)
            if c == b: break
        else:
            invalid_certs.append(a["name"])

    check.equal(list(), invalid_certs)


