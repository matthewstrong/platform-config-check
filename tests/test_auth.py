import pytest
import pytest_check as check
import requests
import logging
from common import copy_left_recursive
from pprint import pformat

@pytest.fixture
def login_options(config, credentials):
    url = "https://{base_url}/tenant/loginOptions".format(base_url=config['url'])
    r = requests.get(url, auth=credentials)
    assert 200 == r.status_code
    payload = r.json()
    check.is_in('loginOptions', payload)
    options_list = payload['loginOptions']
    return options_list

def test_service_user_login(config, credentials):
    url = "https://{base_url}/user/currentUser".format(base_url=config['url'])
    r = requests.get(url, auth=credentials)
    assert 200 == r.status_code

def test_auth_types(config, login_options):
    logging.info("Login Options: %s", pformat(login_options))
    check.is_in('login-options', config)
    check.greater(len(config['login-options']), 0)

    for option_expected in config['login-options']:
        for option_to_test in login_options:
            c = copy_left_recursive(option_expected, option_to_test)
            if c == option_expected: break
        else:
            check.is_true(False, "missing option {}".format(option_expected))