import pyjson5
import pytest
import pytest_check as check
import requests
import logging
from pprint import pformat

def retrieve_tenant_option_group(base_url, credentials, group_name):
    url = 'https://{tenant_url}/tenant/options/{option_group}'.format(tenant_url=base_url, option_group=group_name)
    r = requests.get(url, auth=credentials)
    assert 200 == r.status_code
    return r.json()

def compare_option_group(base_url, credentials, group_name, options_expected):
    options_actual = retrieve_tenant_option_group(base_url, credentials, group_name)
    logging.info("Tenant Options: %s", pformat(options_actual))
    assert sorted(options_expected.items()) == sorted(options_actual.items())

def test_tenant_options(config, option_group, credentials):
    tenant_options = config['tenant_options']
    compare_option_group(config['url'], credentials, option_group, tenant_options[option_group])
