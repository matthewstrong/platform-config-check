import pyjson5
import pytest
from termcolor import colored
import logging

@pytest.fixture(scope="session")
def config(pytestconfig):
    with open(pytestconfig.getoption('config'), 'r') as f:
        config = pyjson5.load(f)
        return config

@pytest.fixture(scope="session")
def credentials(config):
    base_url = config['url']
    with open('config/secrets.json', 'r') as f:
        secrets = pyjson5.load(f)
        creds = secrets['credentials'][base_url]
        return (creds['username'], creds['password'])

@pytest.fixture(scope="module", autouse=True)
def enable_http_logging():
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

def pytest_addoption(parser):
    parser.addoption("--prod", action="store_true",
                     help="run tests that  (marked with marker @production)")
    parser.addoption("--config", action="store", required=True,
                     help="run tests that  (marked with marker @production)")

def pytest_runtest_setup(item):
    if 'production' in item.keywords and not item.config.getoption("--prod"):
        pytest.skip("need --prod option to run this test")

def pytest_generate_tests(metafunc):
    config_file = metafunc.config.getoption('config')
    with open(config_file, 'r') as f:
        config = pyjson5.load(f)
        if 'trusted_cert' in metafunc.fixturenames:
            certs = list(config['trusted-certificates'].keys())
            metafunc.parametrize("trusted_cert", certs)
        if 'app_name' in metafunc.fixturenames:
            apps = list(config['applications'].keys())
            metafunc.parametrize("app_name", apps)
        if 'option_group' in metafunc.fixturenames:
            groups = list(config['tenant_options'].keys())
            metafunc.parametrize("option_group", groups)

def pytest_configure(config):
    config_file = config.getoption('config')
    with open(config_file, 'r') as f:
        json_config = pyjson5.load(f)
        base_url = json_config['url']

    config._metadata["Tenant URL"] = base_url
    config._metadata["Invocation Parameters"] = ' '.join(config.invocation_params.args)

def pytest_report_collectionfinish(config, start_path, startdir, items):
    base_url = config._metadata["Tenant URL"]
    msg = "Running configuration tests against Cumulocity tenant: {}".format(colored(base_url, 'green'))
    return ["", msg]

def pytest_html_report_title(report):
    base_url = report.config._metadata["Tenant URL"]
    report.title = "Test report for {}".format(base_url)